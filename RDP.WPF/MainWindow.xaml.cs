﻿using System;
using System.Management;
using System.Windows;
using System.Windows.Forms.Integration;
using AxMSTSCLib;

namespace RDP.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        AxMsRdpClient8NotSafeForScripting _rdClient;

        private const string RdClientUserName = "RDO";
        private const string RdClientUserPassword = "12345678";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            // Create the host and the ActiveX control
            var host = new WindowsFormsHost();
            _rdClient = new AxMsRdpClient8NotSafeForScripting();

            // Add the ActiveX control to the host, and the host to the WPF panel
            host.Child = _rdClient;
            TiGrid0.Children.Add(host);

            _rdClient.Server = TxtServer.Text.Trim();
            _rdClient.DesktopWidth = _rdClient.Width = (int)TabControl1.Width;
            _rdClient.DesktopHeight = _rdClient.Height = (int)TabControl1.Height;

            _rdClient.UserName = RdClientUserName;
            _rdClient.AdvancedSettings8.ClearTextPassword = RdClientUserPassword;

            _rdClient.AdvancedSettings8.EnableCredSspSupport = true;
            RpcConnect();
        }

        private void StartProcess()
        {
            object[] theProcessToRun = { "notepad.exe" };
            var theConnection = new ConnectionOptions
            {
                Username = RdClientUserName,
                Password = RdClientUserPassword
            };
            var theScope = new ManagementScope("\\\\" + TxtServer.Text.Trim() + "\\root\\cimv2", theConnection);
            var theClass = new ManagementClass(theScope, new ManagementPath("Win32_Process"), new ObjectGetOptions());
            theClass.InvokeMethod("Create", theProcessToRun);
        }
        
        private void RpcConnect()
        {
            try
            {
                _rdClient.Connect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void ButtonProcess_Click(object sender, RoutedEventArgs e)
        {
            StartProcess();
        }
    }
}
